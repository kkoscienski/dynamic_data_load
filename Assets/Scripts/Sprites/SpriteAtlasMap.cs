﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SpriteAtlasMap : ScriptableObject
{
    public const string SPRITE_ATLAS_FOLDER = "SpriteAtlas/";
    public const string BUNDLED_ASSETS_PATH = "Assets/BundledAssets/" + SPRITE_ATLAS_FOLDER;
    public const string SPRITE_ATLAS_MAP_NAME = "SpriteAtlasMap";
    public const string SPRITE_ATLAS_EXT = ".spriteatlas";

    [Serializable]
    public class SpriteAtlasInfo
    {
        public string Tag = "";
        public List<string> SpriteNames = new List<string>();
    }

    public List<SpriteAtlasInfo> Infos = new List<SpriteAtlasInfo>();

    private readonly Dictionary<string, string> m_Tags = new Dictionary<string, string>();

    public bool Parse()
    {
        m_Tags.Clear();
        bool ok = true;
        string spriteName, tag, oldTag;
        for (int i = 0, icount = Infos.Count, s, scount; i < icount; ++i)
        {
            tag = Infos[i].Tag;
            for (s = 0, scount = Infos[i].SpriteNames.Count; s < scount; ++s)
            {
                spriteName = Infos[i].SpriteNames[s];

                if (m_Tags.TryGetValue(spriteName, out oldTag))
                {
                    Debug.LogWarning("Double sprite entry " + spriteName + ", old tag:" + oldTag + ", new tag: " + tag);
                    ok = false;
                }
                m_Tags[spriteName] = tag;
            }
        }
        return ok;
    }

    public string GetTag(string spriteName)
    {
        string path;
        m_Tags.TryGetValue(spriteName, out path);
        return path;
    }
}