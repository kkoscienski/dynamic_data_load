﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

[RequireComponent(typeof(Image))]
public class SpriteLoader : MonoBehaviour
{
    public static readonly Color UNLOADED_COLOR = Color.clear;
    private Color m_OriginalColor = Color.white;

    private Image m_Image = null;
    private string m_SpriteName = "";
    private SpriteAtlasReference m_SpriteAtlasReference = null;
    private SpriteAtlas m_SpriteAtlas = null;

    [SerializeField]
    private bool m_LoadOnStart = true;
    private string m_LoadingSpriteName = "";

    private void Awake()
    {
        m_Image = GetComponent<Image>();
        m_OriginalColor = m_Image.color;
        if (m_Image.sprite)
        {
            m_SpriteName = m_Image.sprite.name;
        }
    }

    private void Start()
    {
        if (m_LoadOnStart)
        {
            Load();
        }
        else
        {
            Unload();
        }

        if (QualityManager.Instance)
        {
            QualityManager.Instance.OnQualityChanged += OnQualityChanged;
        }
    }

    private void OnQualityChanged(Quality quality)
    {
        if (m_SpriteAtlasReference != null)
        {
            Unload();
            Load();
        }
    }

    private void OnDestroy()
    {
        if (QualityManager.Instance)
        {
            QualityManager.Instance.OnQualityChanged -= OnQualityChanged;
        }
        Unload();
    }

    private void OnSpriteAtlasLoaded(SpriteAtlasReference spriteAtlasReference)
    {
        if (spriteAtlasReference != null)
        {
            SpriteAtlas spriteAtlas = spriteAtlasReference.RegisterUser();
            if (spriteAtlas)
            {
                m_SpriteAtlas = spriteAtlas;
                m_Image.sprite = spriteAtlas.GetSprite(m_SpriteName);
                m_Image.color = m_OriginalColor;
            }
            m_SpriteAtlasReference = spriteAtlasReference;
        }
        m_LoadingSpriteName = string.Empty;
    }

    public void SetSpriteName(string spriteName)
    {
        m_SpriteName = spriteName;
    }

    public void Load()
    {
        if (m_SpriteAtlas)
        {
            Sprite sprite = m_SpriteAtlas.GetSprite(m_SpriteName);
            if (sprite != null)
            {
                m_Image.sprite = sprite;
                return;
            }
        }

        Unload();
        m_LoadingSpriteName = m_SpriteName;
        SpriteAtlasManager.Instance.LoadSprite(m_SpriteName, OnSpriteAtlasLoaded);
    }

    public void Unload()
    {
        if (!string.IsNullOrEmpty(m_LoadingSpriteName))
        {
            SpriteAtlasManager.Instance.CancelLoadSprite(m_LoadingSpriteName, OnSpriteAtlasLoaded);
            m_LoadingSpriteName = string.Empty;
        }

        if (m_Image)
        {
            m_Image.sprite = null;
            m_Image.color = UNLOADED_COLOR;
        }
        
        if (m_SpriteAtlasReference != null)
        {
            m_SpriteAtlas = null;
            m_SpriteAtlasReference.UnregisterUser();
            m_SpriteAtlasReference = null;
        }
    }
}
