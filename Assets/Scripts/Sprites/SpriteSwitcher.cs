﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SpriteSwitcher : MonoBehaviour
{
    [SerializeField]
    private Sprite[] m_Sprites = new Sprite[0];

    private SpriteLoader m_SpriteLoader = null;
    private SpriteAtlasLoader m_SpriteAtlasLoader = null;
    private Image m_Image = null;
    private int m_CurrentSpriteIndex = -1;

    private void Awake()
    {
        m_SpriteLoader = GetComponent<SpriteLoader>();
        m_SpriteAtlasLoader = GetComponent<SpriteAtlasLoader>();
        m_Image = GetComponent<Image>();
    }

    public void SetNextSprite()
    {
        SetSprite(++m_CurrentSpriteIndex);
    }

    public void SetPrevSprite()
    {
        SetSprite(--m_CurrentSpriteIndex);
    }

    public void SetRandom()
    {
        int index = Random.Range(0, m_Sprites.Length);
        SetSprite(index);
    }

    public void SetSprite(int index)
    {
        if (index >= m_Sprites.Length)
        {
            index = 0;
        }

        if (index < 0)
        {
            index = m_Sprites.Length -1;
        }

        m_CurrentSpriteIndex = index;

        if (m_CurrentSpriteIndex >= 0 && m_CurrentSpriteIndex < m_Sprites.Length && m_Sprites[m_CurrentSpriteIndex])
        {
            if (m_SpriteLoader)
            {
                m_SpriteLoader.SetSpriteName(m_Sprites[m_CurrentSpriteIndex].name);
                m_SpriteLoader.Load();
            }
            else if (m_SpriteAtlasLoader)
            {
                m_SpriteAtlasLoader.SetSprite(m_Sprites[m_CurrentSpriteIndex].name);
            }
            else if (m_Image)
            {
                m_Image.sprite = m_Sprites[m_CurrentSpriteIndex];
            }
        }
    }
}
