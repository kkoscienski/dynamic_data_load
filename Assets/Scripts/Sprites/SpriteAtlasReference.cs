﻿using System;
using UnityEngine.Events;
using UnityEngine.U2D;

public class SpriteAtlasReference
{
    public delegate void OnZeroReference(SpriteAtlasReference spriteAtlasReference, SpriteAtlas spriteAtlas);
    //public delegate void OnReferenceCountChanged(SpriteAtlasReference spriteAtlasReference);

    public readonly string Path;
    public readonly string AssetBundleName;

    public uint Count
    {
        get { return m_Count; }
    }

    private uint m_Count = 0;

    private readonly OnZeroReference m_Callback;

    private readonly SpriteAtlas m_SpriteAtlas;

    private readonly UnityEventSpriteAtlasReference m_OnReferenceCountChanged = new UnityEventSpriteAtlasReference();

    public event UnityAction<SpriteAtlasReference> OnReferenceCountChanged
    {
        add { m_OnReferenceCountChanged.AddListener(value); }
        remove { m_OnReferenceCountChanged.RemoveListener(value); }
    }

    public SpriteAtlasReference(SpriteAtlas spriteAtlas, string path, string assetBundleName, OnZeroReference callback)
    {
        Path = path;
        AssetBundleName = assetBundleName;
        m_Callback = callback;
        m_SpriteAtlas = spriteAtlas;
    }

    public SpriteAtlas RegisterUser()
    {
        ++m_Count;
        m_OnReferenceCountChanged.Invoke(this);
        return m_SpriteAtlas;
    }

    public void UnregisterUser()
    {
        if (m_Count > 0)
        {
            --m_Count;
            m_OnReferenceCountChanged.Invoke(this);
        }

        if (m_Count == 0)
        {
            if (m_Callback != null)
            {
                m_Callback.Invoke(this, m_SpriteAtlas);
            }
        }
    }
}
