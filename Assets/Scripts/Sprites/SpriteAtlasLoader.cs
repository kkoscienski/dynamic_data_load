﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

[RequireComponent(typeof(Image))]
public class SpriteAtlasLoader : MonoBehaviour
{
    [SerializeField]
    private SpriteAtlas m_SpriteAtlas = null;

    [SerializeField]
    private bool m_LoadOnAwake = true;

    private Image m_Image = null;
    private Color m_OriginalColor = Color.white;

    private void Awake()
    {
        m_Image = GetComponent<Image>();
        m_OriginalColor = m_Image.color;
        if (m_LoadOnAwake && m_Image.sprite)
        {
            SetSprite(m_Image.sprite.name);
        }
        else
        {
            UnloadSprite();
        }
    }
    
    private Sprite GetSprite(string spriteName)
    {
        if (m_SpriteAtlas)
        {
            return m_SpriteAtlas.GetSprite(spriteName);
        }
        return null;
    }

    public void SetSprite(string spriteName)
    {
        m_Image.sprite = GetSprite(spriteName);
        m_Image.color = m_OriginalColor;
    }

    public void UnloadSprite()
    {
        m_Image.sprite = null;
        m_Image.color = SpriteLoader.UNLOADED_COLOR;
    }
}
