﻿using UnityEngine;

public class SpriteLoaderGroup : MonoBehaviour
{
    [SerializeField]
    private SpriteLoader[] m_SpriteLoaders = new SpriteLoader[0];

    public void Load()
    {
        for (int i = 0; i < m_SpriteLoaders.Length; ++i)
        {
            if (m_SpriteLoaders[i])
            {
                m_SpriteLoaders[i].Load();
            }
        }
    }

    public void Unload()
    {
        for (int i = 0; i < m_SpriteLoaders.Length; ++i)
        {
            if (m_SpriteLoaders[i])
            {
                m_SpriteLoaders[i].Unload();
            }
        }
    }
}
