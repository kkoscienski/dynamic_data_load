﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class SceneManager : Singleton<SceneManager>
{
    [SerializeField]
    private UnityEventString m_OnActiveSceneChanged = new UnityEventString();

    private float m_SceneChangeStartTime = 0.0f;

    private readonly Dictionary<string, float> m_TotalTimes = new Dictionary<string, float>();
    private readonly Dictionary<string, int> m_TotalLoads = new Dictionary<string, int>();

    private bool m_ChangingScene = false;
    private AsyncOperation m_LoadSceneAsyncOp = null;
    private string m_SceneAssetBundle = "";

    public void ChangeScene(string sceneName)
    {
        if (!m_ChangingScene)
        {
            ChangeSceneInternal(sceneName, true);
        }
    }

    public void ChangeSceneBundle(string sceneName)
    {
        if (!m_ChangingScene)
        {
            string assetBundleName = "scenes/" + sceneName.ToLowerInvariant();
            m_SceneAssetBundle = assetBundleName;
            AssetBundlesManager.Instance.LoadAssetBundle(assetBundleName, (x) => ChangeSceneInternal(sceneName, true));
            m_ChangingScene = true;
        }
    }

    private void ChangeSceneInternal(string sceneName, bool isAssetBundle)
    {
        m_SceneChangeStartTime = Time.realtimeSinceStartup;
        m_LoadSceneAsyncOp = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
    }

    protected override void OnAwake()
    {
        base.OnAwake();
        UnityEngine.SceneManagement.SceneManager.activeSceneChanged += OnActiveSceneChanged;
        m_OnActiveSceneChanged.Invoke(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }

    protected override void OnDestroy()
    {
        UnityEngine.SceneManagement.SceneManager.activeSceneChanged -= OnActiveSceneChanged;
        base.OnDestroy();
    }

    private void Update()
    {
        if (m_LoadSceneAsyncOp != null && m_LoadSceneAsyncOp.isDone)
        {
            m_LoadSceneAsyncOp = null;
            m_ChangingScene = false;
            if (!string.IsNullOrEmpty(m_SceneAssetBundle))
            {
                AssetBundlesManager.Instance.UnloadAssetBundle(m_SceneAssetBundle, false);
                m_SceneAssetBundle = string.Empty;
            }
        }
    }

    private void OnActiveSceneChanged(Scene from, Scene to)
    {
        float changeDuration = Time.realtimeSinceStartup - m_SceneChangeStartTime;
        m_SceneChangeStartTime = 0.0f;

        string sceneName = to.name;

        float totalTime;
        m_TotalTimes.TryGetValue(sceneName, out totalTime);
        totalTime += changeDuration;
        m_TotalTimes[sceneName] = totalTime;

        int totalLoads;
        m_TotalLoads.TryGetValue(sceneName, out totalLoads);
        ++totalLoads;
        m_TotalLoads[sceneName] = totalLoads;

        Debug.Log(LOG_NAME + "Scene changed to: " + sceneName + ", duration: " + changeDuration, this);
        m_OnActiveSceneChanged.Invoke(sceneName);
    }
}
