﻿using UnityEngine;

[DisallowMultipleComponent]
public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    protected readonly string LOG_NAME = "[" + typeof(T).Name + "] ";

    public static T Instance
    {
        get;
        private set;
    }
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = gameObject.GetComponent<T>();
            if (transform.parent == null)
            {
                DontDestroyOnLoad(gameObject);
            }
            OnAwake();
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    protected virtual void OnAwake()
    {

    }

    protected virtual void OnDestroy()
    {
        Instance = null;
    }
}
