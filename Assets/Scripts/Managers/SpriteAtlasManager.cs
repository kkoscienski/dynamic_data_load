﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.U2D;

public class SpriteAtlasManager : Singleton<SpriteAtlasManager>
{
    public delegate void LoadCallback(SpriteAtlasReference spriteAtlasReference);

    [SerializeField]
    private UnityEventString m_OnDebugInfoChanged = new UnityEventString();

    private string m_DebugInfo = "";
    
    /// <summary>
    /// sprite name -> callback map
    /// </summary>
    private readonly Dictionary<string, HashSet<LoadCallback>> m_Callbacks = new Dictionary<string, HashSet<LoadCallback>>();
    
    /// <summary>
    /// sprite atlas path (with quality) -> sprite atlas map
    /// </summary>
    private readonly Dictionary<string, SpriteAtlasReference> m_SpriteAtlasesReferences = new Dictionary<string, SpriteAtlasReference>();

    /// <summary>
    /// sprite atlas path (with quality) -> sprite names, for callbacks
    /// </summary>
    private readonly Dictionary<string, HashSet<string>> m_PathToSpritesCallbacks = new Dictionary<string, HashSet<string>>();

    /// <summary>
    /// sprite atlas path (with quality) -> start time map, for analytics
    /// </summary>
    private readonly Dictionary<string, float> m_SpriteAtlasesLoadsTimes = new Dictionary<string, float>();

    private SpriteAtlasMap m_Map = null;

    protected override void OnAwake()
    {
        base.OnAwake();
        UnityEngine.U2D.SpriteAtlasManager.atlasRequested += RequestAtlasCallback;
    }

    private IEnumerator Start()
    {
        ResourceRequest request = Resources.LoadAsync<SpriteAtlasMap>(SpriteAtlasMap.SPRITE_ATLAS_MAP_NAME);
        yield return request;
        m_Map = request.asset as SpriteAtlasMap;
        m_Map.Parse();
        Debug.Log(LOG_NAME + "Map loaded: " + m_Map, this);
    }

    protected override void OnDestroy()
    {
        m_Callbacks.Clear();
        m_SpriteAtlasesReferences.Clear();
        m_PathToSpritesCallbacks.Clear();
        m_SpriteAtlasesLoadsTimes.Clear();

        if (m_Map != null)
        {
            Resources.UnloadAsset(m_Map);
            m_Map = null;
        }
        UnityEngine.U2D.SpriteAtlasManager.atlasRequested -= RequestAtlasCallback;
        base.OnDestroy();
    }

    private void RequestAtlasCallback(string tag, Action<SpriteAtlas> action)
    {
        Debug.Log(LOG_NAME + "Unity is requesting sprite atlas: " + tag, this);
    }

    private static string GetAssetBundleName(string spriteAtlasTag, Quality quality)
    {
        return (SpriteAtlasMap.SPRITE_ATLAS_FOLDER + spriteAtlasTag + "." + quality.GetString()).ToLowerInvariant();
    }
    
    private static string GetSpriteAtlasPath(string spriteAtlasTag, Quality quality)
    {
        return SpriteAtlasMap.BUNDLED_ASSETS_PATH + spriteAtlasTag + "_" + quality.GetString() + SpriteAtlasMap.SPRITE_ATLAS_EXT;
    }

    public void LoadSprite(string spriteName, LoadCallback callback)
    {
        if (callback != null)
        {
            if (!string.IsNullOrEmpty(spriteName))
            {
                SpriteAtlasReference spriteAtlasReference;
                Quality quality = QualityManager.Instance.Quality;
                string tag = m_Map.GetTag(spriteName);
                string spriteAtlasPath = GetSpriteAtlasPath(tag, quality);
                if (m_SpriteAtlasesReferences.TryGetValue(spriteAtlasPath, out spriteAtlasReference))
                {
                    callback.Invoke(spriteAtlasReference);
                    GenerateInfo();
                }
                else
                {
                    HashSet<LoadCallback> callbacks;
                    if (!m_Callbacks.TryGetValue(spriteName, out callbacks))
                    {
                        callbacks = new HashSet<LoadCallback>();
                        m_Callbacks.Add(spriteName, callbacks);
                    }

                    if (!callbacks.Contains(callback))
                    {
                        callbacks.Add(callback);
                    }

                    HashSet<string> spriteNames;
                    if (!m_PathToSpritesCallbacks.TryGetValue(spriteAtlasPath, out spriteNames))
                    {
                        spriteNames = new HashSet<string>();
                        m_PathToSpritesCallbacks.Add(spriteAtlasPath, spriteNames);
                    }

                    if (!spriteNames.Contains(spriteName))
                    {
                        spriteNames.Add(spriteName);
                    }

                    string assetBundleName = GetAssetBundleName(tag, quality);
                    AssetBundlesManager.Instance.LoadAssetBundle(assetBundleName, (x) => OnAssetBundleLoaded(assetBundleName, spriteAtlasPath) );
                }
                return;
            }

            callback.Invoke(null);
        }
    }

    private void OnAssetBundleLoaded(string assetBundleName, string spriteAtlasPath)
    {
        m_SpriteAtlasesLoadsTimes[assetBundleName] = Time.realtimeSinceStartup;
        AssetBundlesManager.Instance.LoadObject(assetBundleName, spriteAtlasPath, (x, y) => OnSpriteAtlasLoaded(x, assetBundleName, y));
    }

    private void OnSpriteAtlasLoaded(string spriteAtlasPath, string assetBundleName, UnityEngine.Object obj)
    {
        SpriteAtlas spriteAtlas = obj as SpriteAtlas;
        SpriteAtlasReference spriteAtlasReference = null;

        if (spriteAtlas != null && !string.IsNullOrEmpty(spriteAtlasPath) && !m_SpriteAtlasesReferences.TryGetValue(spriteAtlasPath, out spriteAtlasReference))
        {
            Debug.Log(LOG_NAME + "Loaded sprite atlas: " + spriteAtlasPath, this);
            spriteAtlasReference = new SpriteAtlasReference(spriteAtlas, spriteAtlasPath, assetBundleName, OnZeroReferenceCount);
            m_SpriteAtlasesReferences.Add(spriteAtlasPath, spriteAtlasReference);
            spriteAtlasReference.OnReferenceCountChanged += OnSpriteAtlasReferenceCountChanged;

            float startTime;
            if (m_SpriteAtlasesLoadsTimes.TryGetValue(spriteAtlasPath, out startTime))
            {
                m_SpriteAtlasesLoadsTimes.Remove(spriteAtlasPath);
                float duration = Time.realtimeSinceStartup - startTime;
                AnalyticsManager.Instance.TrackSpriteAtlasLoaded(spriteAtlasPath, duration);
            }

            GenerateInfo();
        }

        HashSet<string> spriteNames;
        if (m_PathToSpritesCallbacks.TryGetValue(spriteAtlasPath, out spriteNames))
        {
            LoadCallback callback = null;
            HashSet<LoadCallback> callbacks;
            foreach (string spriteName in spriteNames)
            {
                if (m_Callbacks.TryGetValue(spriteName, out callbacks))
                {
                    foreach (LoadCallback lc in callbacks)
                    {
                        callback += lc;
                    }
                    m_Callbacks.Remove(spriteName);
                }
            }

            m_PathToSpritesCallbacks.Remove(spriteAtlasPath);
            if (callback != null)
            {
                callback.Invoke(spriteAtlasReference);
            }
        }

        if (spriteAtlasReference != null && spriteAtlasReference.Count == 0)
        {
            OnZeroReferenceCount(spriteAtlasReference, spriteAtlas);
        }
    }

    private void GenerateInfo()
    {
        string debugInfo = LOG_NAME;

        if (m_SpriteAtlasesReferences.Count > 0)
        {
            debugInfo += "\n\tSpriteAtlases:";
            foreach (SpriteAtlasReference spriteAtlasReference in m_SpriteAtlasesReferences.Values)
            {
                debugInfo += "\n\t\t" + spriteAtlasReference.Path + " refCount: " + spriteAtlasReference.Count;
            }
        }
            
        if (debugInfo != m_DebugInfo)
        {
            m_DebugInfo = debugInfo;
            m_OnDebugInfoChanged.Invoke(m_DebugInfo);
        }
    }

    public void CancelLoadSprite(string spriteName, LoadCallback callback)
    {
        if (!string.IsNullOrEmpty(spriteName) && callback != null)
        {
            HashSet<LoadCallback> callbacks;
            if (m_Callbacks.TryGetValue(spriteName, out callbacks))
            {
                if (callbacks.Remove(callback) && callbacks.Count == 0)
                {
                    m_Callbacks.Remove(spriteName);
                }
            }
        }
    }

    private void OnSpriteAtlasReferenceCountChanged(SpriteAtlasReference spriteAtlasReference)
    {
        GenerateInfo();
    }

    private void OnZeroReferenceCount(SpriteAtlasReference spriteAtlasReference, SpriteAtlas spriteAtlas)
    {
        if (spriteAtlas)
        {
            Debug.Log(LOG_NAME + "Unloaded sprite atlas: " + spriteAtlas.name, this);
        }

        if (spriteAtlasReference != null)
        {
            string spriteAtlasPath = spriteAtlasReference.Path;
            spriteAtlasReference.OnReferenceCountChanged -= OnSpriteAtlasReferenceCountChanged;
            m_SpriteAtlasesReferences.Remove(spriteAtlasPath);
            string assetBundleName = spriteAtlasReference.AssetBundleName;
            if (AssetBundlesManager.Instance) //quit game check
            {
                AssetBundlesManager.Instance.UnloadAssetBundle(assetBundleName, true);
            }
        }
        GenerateInfo();
    }
}
