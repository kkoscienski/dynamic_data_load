﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AssetBundlesManager : Singleton<AssetBundlesManager>
{
    public delegate void LoadAssetBundleCallback(string assetBundleName);
    public delegate void LoadAssetCallback(string assetPath, Object asset);

    [SerializeField]
    private UnityEventString m_OnDebugInfoChanged = new UnityEventString();

    private string m_DebugInfo = "";
        

    /// <summary>
    /// asset bundle name -> callback map
    /// </summary>
    private readonly Dictionary<string, HashSet<LoadAssetBundleCallback>> m_AssetBundlesLoadsCallbacks = new Dictionary<string, HashSet<LoadAssetBundleCallback>>();

    /// <summary>
    /// asset path -> load object callback
    /// </summary>
    private readonly Dictionary<string, HashSet<LoadAssetCallback>> m_AssetsLoadsCallbacks = new Dictionary<string, HashSet<LoadAssetCallback>>();

    /// <summary>
    /// sprite atlas path (with quality) -> start time map, for analytics
    /// </summary>
    private readonly Dictionary<string, float> m_AssetBundlesLoadsTimes = new Dictionary<string, float>();

    private readonly Dictionary<string, UnityWebRequest> m_AssetBundlesLoads = new Dictionary<string, UnityWebRequest>();
    private readonly Dictionary<string, AssetBundle> m_AssetBundles = new Dictionary<string, AssetBundle>();
    private readonly Dictionary<string, AssetBundleRequest> m_AssetsLoads = new Dictionary<string, AssetBundleRequest>();

    protected override void OnDestroy()
    {
        m_AssetBundlesLoadsCallbacks.Clear();
        m_AssetsLoadsCallbacks.Clear();

        foreach (UnityWebRequest uwr in m_AssetBundlesLoads.Values)
        {
            uwr.Dispose();
        }
        m_AssetBundlesLoads.Clear();

        foreach (AssetBundle ab in m_AssetBundles.Values)
        {
            ab.Unload(true);
        }
        m_AssetBundles.Clear();
        m_AssetsLoads.Clear();

        base.OnDestroy();
    }

    public static string GetAssetBundlesPath()
    {
        return Application.streamingAssetsPath + "/AssetBundles/";
    }

    public void LoadAssetBundle(string assetBundleName, LoadAssetBundleCallback callback)
    {
        if (callback != null)
        {
            if (string.IsNullOrEmpty(assetBundleName) || m_AssetBundles.ContainsKey(assetBundleName))
            {
                callback.Invoke(assetBundleName);
                return;
            }

            HashSet<LoadAssetBundleCallback> callbacks;
            if (!m_AssetBundlesLoadsCallbacks.TryGetValue(assetBundleName, out callbacks))
            {
                callbacks = new HashSet<LoadAssetBundleCallback>();
                m_AssetBundlesLoadsCallbacks.Add(assetBundleName, callbacks);
            }

            callbacks.Add(callback);

            if (!m_AssetBundlesLoads.ContainsKey(assetBundleName))
            {
                string assetBundlePath = GetAssetBundlesPath() + assetBundleName;
                UnityWebRequest uwr = UnityWebRequest.GetAssetBundle(assetBundlePath);
                uwr.Send();
                m_AssetBundlesLoads.Add(assetBundleName, uwr);
                m_AssetBundlesLoadsTimes[assetBundleName] = Time.realtimeSinceStartup;
            }
        }
    }

    public void UnloadAssetBundle(string assetBundleName, bool unloadAllLoadedObjects)
    {
        AssetBundle assetBundle;
        if (!string.IsNullOrEmpty(assetBundleName) && m_AssetBundles.TryGetValue(assetBundleName, out assetBundle))
        {
            m_AssetBundles.Remove(assetBundleName);
            if (assetBundle)
            {
                Debug.Log(LOG_NAME + "Unloaded asset bundle: " + assetBundle.name, this);
                assetBundle.Unload(unloadAllLoadedObjects);
            }
            GenerateInfo();
        }
    }

    public void LoadObject(string assetBundleName, string assetPath, LoadAssetCallback callback)
    {
        if (callback != null)
        {
            AssetBundle assetBundle;
            if (!string.IsNullOrEmpty(assetBundleName) &&
                !string.IsNullOrEmpty(assetPath) &&
                m_AssetBundles.TryGetValue(assetBundleName, out assetBundle))
            {
                HashSet<LoadAssetCallback> callbacks;
                if (!m_AssetsLoadsCallbacks.TryGetValue(assetPath, out callbacks))
                {
                    callbacks = new HashSet<LoadAssetCallback>();
                    m_AssetsLoadsCallbacks.Add(assetPath, callbacks);
                }
                callbacks.Add(callback);

                if (!m_AssetsLoads.ContainsKey(assetPath))
                {
                    AssetBundleRequest assetBundleRequest = assetBundle.LoadAssetAsync(assetPath);
                    m_AssetsLoads.Add(assetPath, assetBundleRequest);
                }
            }
            else
            {
                callback.Invoke(assetPath, null);
            }
        }
    }

    private void Update()
    {
        if (m_AssetBundlesLoads.Count > 0)
        {
            string assetBundleName = string.Empty;
            AssetBundle assetBundle;
            foreach (KeyValuePair<string, UnityWebRequest> kvp in m_AssetBundlesLoads)
            {
                if (kvp.Value.isDone)
                {
                    assetBundleName = kvp.Key;
                    assetBundle = ((DownloadHandlerAssetBundle)kvp.Value.downloadHandler).assetBundle;

                    Debug.Log(LOG_NAME + "Loaded asset bundle: " + assetBundleName, this);
                    float startTime;
                    if (m_AssetBundlesLoadsTimes.TryGetValue(assetBundleName, out startTime))
                    {
                        m_AssetBundlesLoadsTimes.Remove(assetBundleName);
                        float duration = Time.realtimeSinceStartup - startTime;
                        AnalyticsManager.Instance.TrackAssetBundleLoaded(assetBundleName, duration);
                    }

                    m_AssetBundles.Add(assetBundleName, assetBundle);
                    kvp.Value.Dispose();
                    break;
                }
            }

            if (!string.IsNullOrEmpty(assetBundleName))
            {
                m_AssetBundlesLoads.Remove(assetBundleName);
                LoadAssetBundleCallback callback = null;
                HashSet<LoadAssetBundleCallback> callbacks;
                if (m_AssetBundlesLoadsCallbacks.TryGetValue(assetBundleName, out callbacks))
                {
                    foreach (LoadAssetBundleCallback labc in callbacks)
                    {
                        callback += labc;
                    }
                    m_AssetBundlesLoadsCallbacks.Remove(assetBundleName);
                }

                if (callback != null)
                {
                    callback.Invoke(assetBundleName);
                }
            }
            GenerateInfo();
        }

        if (m_AssetsLoads.Count > 0)
        {
            string assetPath = string.Empty;
            Object asset = null;
            foreach (KeyValuePair<string, AssetBundleRequest> kvp in m_AssetsLoads)
            {
                if (kvp.Value.isDone)
                {
                    assetPath = kvp.Key;
                    asset = kvp.Value.asset;
                    break;
                }
            }

            if (!string.IsNullOrEmpty(assetPath))
            {
                m_AssetsLoads.Remove(assetPath);
                LoadAssetCallback callback = null;
                HashSet<LoadAssetCallback> callbacks;
                if (m_AssetsLoadsCallbacks.TryGetValue(assetPath, out callbacks))
                {
                    foreach (LoadAssetCallback lac in callbacks)
                    {
                        callback += lac;
                    }
                    m_AssetsLoadsCallbacks.Remove(assetPath);
                }

                if (callback != null)
                {
                    callback.Invoke(assetPath, asset);
                }
            }
            GenerateInfo();
        }
    }

    private void GenerateInfo()
    {
        string debugInfo = LOG_NAME;
        if (m_AssetBundlesLoads.Count > 0)
        {
            debugInfo += "\n\tLoading asset bundles:";
            foreach (UnityWebRequest uwr in m_AssetBundlesLoads.Values)
            {
                debugInfo += "\n\t\t" + uwr.url + " " + Mathf.CeilToInt(uwr.downloadProgress * 100.0f) + "%";
            }
        }
            
        if (m_AssetBundles.Count > 0)
        {
            debugInfo += "\n\tAsset bundles:";
            foreach (AssetBundle assetBundle in m_AssetBundles.Values)
            {
                debugInfo += "\n\t\t" + assetBundle.name;
            }
        }
            
        if (m_AssetsLoads.Count > 0)
        {
            debugInfo += "\n\tLoading assets:";
            foreach (KeyValuePair<string, AssetBundleRequest> kvp in m_AssetsLoads)
            {
                debugInfo += "\n\t\t" + kvp.Key + " " + Mathf.CeilToInt(kvp.Value.progress * 100.0f) + "%";
            }
        }

        if (debugInfo != m_DebugInfo)
        {
            m_DebugInfo = debugInfo;
            m_OnDebugInfoChanged.Invoke(m_DebugInfo);
        }
    }
}
