﻿using System.Collections.Generic;

#if USE_ANALYTICS
using UnityEngine;
using UnityEngine.Analytics;
#endif //USE_ANALYTICS

public class AnalyticsManager : Singleton<AnalyticsManager>
{
    private const string AB_LOADED_EVENT = "assetBundleLoaded";
    private const string SA_LOADED_EVENT = "spriteAtlasLoaded";
    private const string SCREEN_VISIT_EVENT = "screenVisit";

    private const string NAME_KEY = "name";
    private const string PATH_KEY = "path";
    private const string DURATION_KEY = "duration";

#if USE_ANALYTICS
    private void Start()
    {
        Analytics.enabled = true;
        Analytics.deviceStatsEnabled = true;
        Analytics.limitUserTracking = false;
    }

    protected override void OnDestroy()
    {
        Analytics.FlushEvents();
        base.OnDestroy();
    }
#endif //USE_ANALYTICS

    public void TrackAssetBundleLoaded(string assetBundleName, float duration)
    {
        if (!string.IsNullOrEmpty(assetBundleName) && duration >= 0.0f)
        {
            TrackCustomEvent(AB_LOADED_EVENT,
                new Dictionary<string, object>()
                {
                    { NAME_KEY, assetBundleName },
                    { DURATION_KEY, duration }
                });
        }
    }

    public void TrackSpriteAtlasLoaded(string spriteAtlasPath, float duration)
    {
        if (!string.IsNullOrEmpty(spriteAtlasPath) && duration >= 0.0f)
        {
            TrackCustomEvent(SA_LOADED_EVENT,
                new Dictionary<string, object>()
                {
                    { PATH_KEY, spriteAtlasPath },
                    { DURATION_KEY, duration }
                });
        }
    }

    public void TrackScreenVisit(string screenName)
    {
        if (!string.IsNullOrEmpty(screenName))
        {
            TrackCustomEvent(SCREEN_VISIT_EVENT,
                new Dictionary<string, object>()
                {
                    { NAME_KEY, screenName }
                });
        }
    }
    
    private void TrackCustomEvent(string customEventName, IDictionary<string, object> eventData)
    {
#if USE_ANALYTICS
        AnalyticsResult analyticsResult = Analytics.CustomEvent(customEventName, eventData);
        if (analyticsResult != AnalyticsResult.Ok)
        {
            Debug.LogWarning(LOG_NAME + "Could not send custom event: " + customEventName + ", reason: " + analyticsResult.ToString(), this);
        }
#endif //USE_ANALYTICS
    }
}
