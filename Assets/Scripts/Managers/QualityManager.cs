﻿using UnityEngine;
using UnityEngine.Events;

public enum Quality
{
    Invalid,
    Normal,
    High,
    Low
}

public static class QualityExtension
{
    private const string INVALID = "Invalid";
    private const string NORMAL = "Normal";
    private const string HIGH = "High";
    private const string LOW = "Low";

    public static string GetString(this Quality quality)
    {
        switch (quality)
        {
            case Quality.Normal: return NORMAL;
            case Quality.High: return HIGH;
            case Quality.Low: return LOW;
            default: return INVALID;
        }
    }

    public static string ToAssetBundleVariant(this Quality quality)
    {
        return quality.ToString().ToLowerInvariant();
    }

    public static Quality ToQuality(this string qualityString)
    {
        switch (qualityString)
        {
            case NORMAL: return Quality.Normal;
            case HIGH: return Quality.High;
            case LOW: return Quality.Low;
            default: return Quality.Invalid;
        }
    }
}


public class QualityManager : Singleton<QualityManager>
{
    private const string QUALITY_KEY = "Quality";
    private const Quality DEFAULT_QUALITY = Quality.Normal;

    private Quality m_Quality = Quality.Invalid;

    [SerializeField]
    private UnityEventQuality m_OnQualityChanged = new UnityEventQuality();

    [SerializeField]
    private UnityEventString m_OnQualityChangedStr = new UnityEventString();

    public event UnityAction<Quality> OnQualityChanged
    {
        add { m_OnQualityChanged.AddListener(value); }
        remove { m_OnQualityChanged.RemoveListener(value); }
    }

    public event UnityAction<string> OnQualityChangedStr
    {
        add { m_OnQualityChangedStr.AddListener(value); }
        remove { m_OnQualityChangedStr.RemoveListener(value); }
    }

    public Quality Quality
    {
        get { return m_Quality; }
    }

    protected override void OnAwake()
    {
        base.OnAwake();
        Load();
    }

    public void SetQuality(Quality quality)
    {
        if (m_Quality != quality && quality != Quality.Invalid)
        {
            Debug.Log(LOG_NAME + "Quality changed from: " + m_Quality.GetString() + " to: " + quality.GetString(), this);
            m_Quality = quality;
            Save();
            m_OnQualityChanged.Invoke(m_Quality);
            m_OnQualityChangedStr.Invoke(m_Quality.GetString());
        }
    }

    public void SetQuality(string quality)
    {
        SetQuality(quality.ToQuality());
    }

    private void Save()
    {
        PlayerPrefs.SetInt(QUALITY_KEY, (int)m_Quality);
        PlayerPrefs.Save();
    }

    private void Load()
    {
        SetQuality((Quality)PlayerPrefs.GetInt(QUALITY_KEY, (int)DEFAULT_QUALITY));
    }
}
