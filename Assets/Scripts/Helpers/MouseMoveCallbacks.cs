﻿using UnityEngine;
using UnityEngine.Events;

public class MouseMoveCallbacks : MonoBehaviour
{
    [SerializeField]
    private UnityEvent m_OnMouseMoved = new UnityEvent();

    private Vector3 m_MousePosition = new Vector2();

    private void Start()
    {
        m_MousePosition = Input.mousePosition;
    }

    private void Update()
    {
		if (Vector3.Distance(Input.mousePosition, m_MousePosition) > 2.0f)
        {
            m_OnMouseMoved.Invoke();
        }

        m_MousePosition = Input.mousePosition;
	}
}
