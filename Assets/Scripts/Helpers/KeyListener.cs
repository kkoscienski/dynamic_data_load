﻿using UnityEngine;
using UnityEngine.Events;

public class KeyListener : MonoBehaviour
{
    [SerializeField]
    private KeyCode m_Keycode = KeyCode.None;

    [SerializeField]
    private UnityEvent m_OnKeyUp = new UnityEvent();

	private void Update()
    {
		if (Input.GetKeyUp(m_Keycode))
        {
            m_OnKeyUp.Invoke();
        }
	}
}
