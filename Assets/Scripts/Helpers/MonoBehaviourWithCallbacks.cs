﻿using UnityEngine;
using UnityEngine.Events;

public class MonoBehaviourWithCallbacks : MonoBehaviour
{
    [SerializeField]
    private UnityEvent m_OnAwake = new UnityEvent();

    [SerializeField]
    private UnityEvent m_OnStart = new UnityEvent();

    [SerializeField]
    private UnityEvent m_OnEnable = new UnityEvent();

    [SerializeField]
    private UnityEvent m_OnDisable = new UnityEvent();

    [SerializeField]
    private UnityEvent m_OnDestroy = new UnityEvent();

    private void Awake()
    {
        m_OnAwake.Invoke();
    }

    private void Start()
    {
        m_OnStart.Invoke();
	}

    private void OnEnable()
    {
        m_OnEnable.Invoke();
    }

    private void OnDisable()
    {
        m_OnDisable.Invoke();
    }
	
	private void OnDestroy()
    {
        m_OnDestroy.Invoke();
	}
}
