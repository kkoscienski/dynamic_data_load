﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class MonoBehaviourWait : MonoBehaviour
{
    [SerializeField]
    private UnityEvent m_OnWaitEnd = new UnityEvent();

    private bool m_IsWaiting = false;

    public void Wait(float seconds)
    {
        Cancel();
        m_IsWaiting = true;
        StartCoroutine(WaitForSeconds(seconds));
    }

    public void Cancel()
    {
        if (m_IsWaiting)
        {
            StopAllCoroutines();
            m_IsWaiting = false;
        }
    }

    private IEnumerator WaitForSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        m_IsWaiting = false;
        m_OnWaitEnd.Invoke();
    }
}
