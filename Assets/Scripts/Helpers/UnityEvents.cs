﻿using System;
using UnityEngine.Events;

[Serializable]
public class UnityEventString : UnityEvent<string>
{
}

[Serializable]
public class UnityEventInt : UnityEvent<int>
{
}

[Serializable]
public class UnityEventFloat : UnityEvent<float>
{
}

[Serializable]
public class UnityEventBool : UnityEvent<bool>
{
}

[Serializable]
public class UnityEventQuality : UnityEvent<Quality>
{
}

[Serializable]
public class UnityEventSpriteAtlasReference : UnityEvent<SpriteAtlasReference>
{
}