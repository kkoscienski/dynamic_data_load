﻿using UnityEngine.U2D;
using UnityEngine;
using UnityEditor;
using System.IO;

public static class Tools
{
    private const string SOURCE_SPRITEATLASES_PATH = "Assets/Sprites";
    private const string SPRITE_ATLAS_MAP_PATH = "Assets/Resources/" + SpriteAtlasMap.SPRITE_ATLAS_MAP_NAME + ".asset";
    private readonly static string[] SOURCE_FOLDERS = new string[] { SOURCE_SPRITEATLASES_PATH };
    private readonly static string[] BUNDLE_FOLDERS = new string[] { SpriteAtlasMap.BUNDLED_ASSETS_PATH.Substring(0, SpriteAtlasMap.BUNDLED_ASSETS_PATH.Length - 1) };

    [MenuItem("Tools/Prepare everything", priority = 0)]
    public static void PrepareEverything()
    {
        AssignAssetBundles();
        CreateSpriteAtlasMaps();
        BuildAssetBundles(EditorUserBuildSettings.activeBuildTarget);
        CopyAssetBundles();
    }

    [MenuItem("Tools/Prepare everything from scratch", priority = 1)]
    public static void PrepareEverythingFromScratch()
    {
        RemoveAssetBundles();
        AssignAssetBundles();
        CreateSpriteAtlasMaps();
        BuildAssetBundles(EditorUserBuildSettings.activeBuildTarget);
        CopyAssetBundles();
    }

    [MenuItem("Tools/Build asset bundles for StandaloneWindows64", priority = 4)]
    private static void BuildAssetBundlesStandaloneWindows64()
    {
        BuildAssetBundles(BuildTarget.StandaloneWindows64);
    }

    [MenuItem("Tools/Build asset bundles for WebGL", priority = 4)]
    private static void BuildAssetBundlesWebGL()
    {
        BuildAssetBundles(BuildTarget.WebGL);
    }

    [MenuItem("Tools/Copy asset bundles to streaming assets", priority = 5)]
    private static void CopyAssetBundles()
    {
        string assetBundlesTargetPath = GetAssetBundlesTargetPath(EditorUserBuildSettings.activeBuildTarget);
        if (Directory.Exists(assetBundlesTargetPath))
        {
            EnsureDirectoryExists(Application.streamingAssetsPath);
            string destination = AssetBundlesManager.GetAssetBundlesPath();
            FileUtil.DeleteFileOrDirectory(destination);
            FileUtil.CopyFileOrDirectory(assetBundlesTargetPath, destination);
            Debug.Log("Asset bundles copied to: " + destination);
            AssetDatabase.Refresh();
        }
        else
        {
            Debug.LogError("Asset bundles source directory does not exists: " + assetBundlesTargetPath);
        }
    }

    private static void BuildAssetBundles(BuildTarget buildTarget)
    {
        string assetBundlesPath = GetAssetBundlesPath();
        string assetBundlesTargetPath = GetAssetBundlesTargetPath(buildTarget);
        EnsureDirectoryExists(assetBundlesPath);
        EnsureDirectoryExists(assetBundlesTargetPath);
        BuildPipeline.BuildAssetBundles(assetBundlesTargetPath, BuildAssetBundleOptions.None, buildTarget);
        Debug.Log("Asset bundles for buildTarget: " + buildTarget + " builded to: " + assetBundlesTargetPath);
    }

    [MenuItem("Tools/Remove asset bundles", priority = 6)]
    private static void RemoveAssetBundles()
    {
        string assetBundlesPath = GetAssetBundlesTargetPath(EditorUserBuildSettings.activeBuildTarget);
        ClearDirectory(assetBundlesPath);
        Debug.Log("Removed asset bundles from: " + assetBundlesPath);

        assetBundlesPath = AssetBundlesManager.GetAssetBundlesPath();
        ClearDirectory(assetBundlesPath);
        Debug.Log("Removed asset bundles from: " + assetBundlesPath);
        AssetDatabase.Refresh();
    }

    private static void ClearDirectory(string path)
    {
        if (Directory.Exists(path))
        {
            Directory.Delete(path, true);
        }
        EnsureDirectoryExists(path);
    }

    private static void EnsureDirectoryExists(string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
    }

    private static string GetAssetBundlesPath()
    {
        return Application.dataPath.Replace("Assets", "AssetBundles/");
    }

    private static string GetAssetBundlesTargetPath(BuildTarget buildTarget)
    {
        return GetAssetBundlesPath() + buildTarget.ToString() + "/";
    }

    [MenuItem("Tools/Create Sprite Atlas Map", priority = 3)]
    private static void CreateSpriteAtlasMaps()
    {
        string[] guids = AssetDatabase.FindAssets("t:spriteatlas", SOURCE_FOLDERS);

        SpriteAtlasMap asset = AssetDatabase.LoadAssetAtPath<SpriteAtlasMap>(SPRITE_ATLAS_MAP_PATH);

        if (asset == null)
        {
            asset = ScriptableObject.CreateInstance<SpriteAtlasMap>();
            AssetDatabase.CreateAsset(asset, SPRITE_ATLAS_MAP_PATH);
        }

        asset.Infos.Clear();

        SpriteAtlas spriteAtlas;
        SpriteAtlasMap.SpriteAtlasInfo info;
        string spriteAtlasMapPath;
        Sprite[] sprites;
        for (int i = 0, j; i < guids.Length; ++i)
        {
            spriteAtlasMapPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            spriteAtlas = AssetDatabase.LoadAssetAtPath<SpriteAtlas>(spriteAtlasMapPath);

            if (spriteAtlas && spriteAtlasMapPath.EndsWith(spriteAtlas.tag + SpriteAtlasMap.SPRITE_ATLAS_EXT))
            {
                info = new SpriteAtlasMap.SpriteAtlasInfo();
                info.Tag = spriteAtlas.tag;

                sprites = new Sprite[spriteAtlas.spriteCount];
                spriteAtlas.GetSprites(sprites);
                for (j = 0; j < sprites.Length; ++j)
                {
                    info.SpriteNames.Add(sprites[j].name.Replace("(Clone)", ""));
                }
                info.SpriteNames.Sort();
                asset.Infos.Add(info);
            }
        }

        EditorUtility.SetDirty(asset);
        
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;

        if (!asset.Parse())
        {
            Debug.LogWarning("Sprite Atlas Map have warnings " + SPRITE_ATLAS_MAP_PATH, asset);
        }
    }

    [MenuItem("Tools/Assign asset bundles", priority = 2)]
    private static void AssignAssetBundles()
    {
        string[] guids = AssetDatabase.FindAssets("t:spriteatlas", BUNDLE_FOLDERS);

        string spriteAtlasPath;
        SpriteAtlas spriteAtlas;
        string qualityStr, assetBundleName;
        Quality quality;
        int startIndex, length;
        for (int i = 0; i < guids.Length; ++i)
        {
            spriteAtlasPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            spriteAtlas = AssetDatabase.LoadAssetAtPath<SpriteAtlas>(spriteAtlasPath);
            
            if (spriteAtlas)
            {
                startIndex = SpriteAtlasMap.BUNDLED_ASSETS_PATH.Length + spriteAtlas.tag.Length + 1;
                length = spriteAtlasPath.Length - (startIndex + SpriteAtlasMap.SPRITE_ATLAS_EXT.Length);
                qualityStr = spriteAtlasPath.Substring(startIndex, length);
                quality = qualityStr.ToQuality();
                if (quality != Quality.Invalid)
                {
                    assetBundleName = GetAssetBundleName(spriteAtlas.tag);
                    AssignToAssetBundle(spriteAtlasPath, assetBundleName, quality.ToAssetBundleVariant());
                }
            }
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private static string GetAssetBundleName(string spriteAtlasTag)
    {
        return (SpriteAtlasMap.SPRITE_ATLAS_FOLDER + spriteAtlasTag).ToLowerInvariant();
    }

    private static void AssignToAssetBundle(string path, string assetBundleName, string assetBundleVariant)
    {
        if (!string.IsNullOrEmpty(path))
        {
            AssetImporter importer = AssetImporter.GetAtPath(path);
            if (!string.IsNullOrEmpty(assetBundleName))
            {
                assetBundleName = assetBundleName.ToLowerInvariant();
            }

            if (!string.IsNullOrEmpty(assetBundleVariant))
            {
                assetBundleVariant = assetBundleVariant.ToLowerInvariant();
            }
            
            if (importer != null && (importer.assetBundleName != assetBundleName || importer.assetBundleVariant != assetBundleVariant))
            {
                importer.SetAssetBundleNameAndVariant(assetBundleName, assetBundleVariant);
                importer.SaveAndReimport();
            }
        }
    }
}
